const EventEmitter=require("events");
const {PubSub} = require('@google-cloud/pubsub');

class PubsubUpdater extends EventEmitter {
	constructor(params, topic) {
		super();

		if (!params)
			params={};

		this.promises=[];
		this.topic=topic;
		this.pubsub=new PubSub(params);
	}

	setCrawlId(crawlId) {
		this.crawlId=crawlId;
	}

	send(message) {
		let buffer=Buffer.from(JSON.stringify(message));
		let promise=this.pubsub.topic(this.topic).publish(buffer);

		promise.catch((e)=>{
			console.log("pubsub error!");
			this.emit("error",e);
		});

		this.promises.push(promise);
		return promise;
	}

	sendProgress(progress) {
		return this.send({
			_: "progress",
			progress: progress,
			crawlId: this.crawlId
		});
	}

	updateProgress(progress) {
		this.progress=progress;

		if (this.progressCall)
			return;

		this.progressCall=this.sendProgress(progress);
		this.progressCall.then(()=>{
			setTimeout(()=>{
				this.progressCall=null;
				if (this.progress!=progress)
					this.updateProgress(this.progress);
			},2500);
		});
	}

	async flush() {
		await Promise.resolve(Promise.all(this.promises));
	}
}

module.exports=PubsubUpdater;