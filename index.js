const crawly=require("crawly");
const GoogleSheetUpdater=require("./GoogleSheetUpdater.js");
const PubsubUpdater=require("./PubsubUpdater.js");
const PusherUpdater=require("./PusherUpdater.js");

async function runCrawl(params) {
	let crawler=crawly.Crawler.createFromPackage(__dirname+"/node_modules/"+params.crawler);
	let crawlRunner=crawler.createCrawlRunner(/*"fork"*/);

	let crawlRunnerInputs={};
	for (let input of crawler.getInputs())
		crawlRunnerInputs[input.name]=params["input-"+input.name];

	crawlRunner.setInputs(crawlRunnerInputs);

	crawlRunner.setPupArg('--disable-gpu');
	crawlRunner.setPupArg('--disable-dev-shm-usage');
	crawlRunner.setPupArg('--disable-setuid-sandbox');
	crawlRunner.setPupArg('--no-first-run');
	crawlRunner.setPupArg('--no-sandbox');
	crawlRunner.setPupArg('--no-zygote');
	crawlRunner.setPupArg('--single-process');
	crawlRunner.setPupArg('--ignore-certificate-errors');
	crawlRunner.setPupArg('--ignore-certificate-errors-spki-list');
	crawlRunner.setPupArg('--enable-features=NetworkService');

	crawlRunner.setPupOption("ignoreHTTPSErrors",true);

	console.log("using proxy: "+params.proxy);

	if (params.proxy)
		crawlRunner.setPupArg("--proxy-server="+params.proxy);

	console.log("using auth: "+params.auth);

	if (params.auth)
		crawlRunner.setAuth(params.auth);

	let runPromiseResolve,runPromiseReject;
	let runPromise=new Promise((resolve,reject)=>{
		runPromiseResolve=resolve;
		runPromiseReject=reject;
	});

	let sheetUpdater;
	let resultRows=[];

	switch (params.delivery) {
		case "gdoc":
			sheetUpdater=new GoogleSheetUpdater(params.spreadsheetId);
			sheetUpdater.setInterCallDelay(2500);
			sheetUpdater.setClientIdAndSecret(
				params.clientId,
				params.secret
			);

			sheetUpdater.setRefreshToken(params.refreshToken);
			sheetUpdater.setTarget(params.target);
			await sheetUpdater.init();

			sheetUpdater.on("error",(e)=>{
				runPromiseReject(e);
			});
			break;


		case "none":
			break;

		default:
			throw new Error("Unknown delivery method: "+params.delivery);
	}

	let progressUpdater;

	if (params.pubsubTopic) {
		let pubsubParams={};
		if (params.keyFilename)
			pubsubParams.keyFilename=params.keyFilename;

		progressUpdater=new PubsubUpdater(pubsubParams,params.pubsubTopic);
	}

	if (params.pusherChannel) {
		let pusherParams={
			appId: params.pusherAppId,
			key: params.pusherKey,
			secret: params.pusherSecret,
			cluster: params.pusherCluster,
		}

		progressUpdater=new PusherUpdater(pusherParams,params.pusherChannel);
	}

	if (progressUpdater) {
		progressUpdater.on("error",(e)=>{
			runPromiseReject(e);
		});

		progressUpdater.setCrawlId(params.crawlId);
		progressUpdater.updateProgress(0);
	}

	crawlRunner.on("log",(msg)=>{
		console.log("--> "+msg);
	});

	crawlRunner.on("result",(row)=>{
		switch (params.delivery) {
			case "gdoc":
				sheetUpdater.appendObject(row);
				break;

			case "none":
				resultRows.push(row);
				break;

			default:
				throw new Error("Only google docs supported.");
				break;
		}
	});

	crawlRunner.on("progress",(progress)=>{
		if (progressUpdater)
			progressUpdater.updateProgress(progress);
	});

	crawlRunner.run()
		.then(runPromiseResolve)
		.catch(runPromiseReject);

	console.log("Running, waiting for run promise to complete...");
	await runPromise;

	let returnValue;
	switch (params.delivery) {
		case "gdoc":
			console.log("Flushing sheet updater...");
			await sheetUpdater.flush();
			break;

		case "none":
			returnValue=resultRows;
			break;

		default:
			break;
	}

	if (progressUpdater) {
		progressUpdater.send({
			_: "complete",
			crawlId: params.crawlId
		});

		console.log("Flushing progress updater...");
		await progressUpdater.flush();
	}

	console.log("Done!");
	return returnValue;
}

exports.runCrawlRest=(req,res)=>{
	runCrawl(req.query)
		.then((crawlResult)=>{
			let body=JSON.stringify({success: "ok", result: crawlResult});
			res.status(200).send(body);
		})
		.catch((e)=>{
			console.log("Crawl failed: "+e.toString());
			console.log(e);
			res.status(500).send(e.toString());
		});
}

exports.runCrawlPubsub=async(data)=>{
	let params=JSON.parse(Buffer.from(data.data,"base64"));
	return await runCrawl(params);
};