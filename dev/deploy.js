const gcloud=require("gcloud-wrap");
const fs=require("fs");

async function main() {
	let settings=JSON.parse(fs.readFileSync(__dirname+"/../../geckocrawl.settings.json"));

	console.log("Activating service account...");
	await gcloud.auth.activateServiceAccount({
		keyFileContent: settings.service_account,
		setProjectFromKeyFile: true
	});

	console.log("Deploying...");
	let res=await gcloud.functions.deploy("crawly-gcloud-runner",{
		runtime: "nodejs8",
		triggerHttp: true,
		timeout: 540,
		memory: "2G",
		serviceAccount: settings.service_account.client_email,
		entryPoint: "runCrawlRest"
	});

	console.log("Done!");
	console.log(res);
}

main();