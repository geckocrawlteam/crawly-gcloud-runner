const fs=require("fs");

class DummyResponse {
	status(statusCode) {
		this.statusCode=statusCode;
		console.log("** Returning status: "+statusCode)
		return this;
	}

	send(body) {
		this.body=body;
		console.log("** Returning body: "+body)
		return this;
	}
}

async function main() {
	let runner=require("../index.js");

	let settings=JSON.parse(fs.readFileSync(__dirname+"/../../geckocrawl.settings.json"));

	let query={
		crawler: "agoda-crawler",
		delivery: "gdoc",
		clientId: settings.google.clientId,
		secret: settings.google.secret,
		target: "MickeTest!A1",
		refreshToken: "1/UKJHmi0xHfiFjkoWG4zAh1Rm_RpkatLV_MNBLI4xzpQ",
		spreadsheetId: "1tZ4LY_IeLHfeGcn-LbgfUDKt8PCdUWJOoX5-Qlrb8wA",
		pusherAppId: settings.pusher.appId,
		pusherKey: settings.pusher.key,
		pusherSecret: settings.pusher.secret,
		pusherCluster: settings.pusher.cluster,
		pusherChannel: "crawl-progress",
		crawlId: "123",
		"input-region": "bali",
		"input-results": 10,
		"input-rooms": 1,
		"input-daysfromnow": 7
	};

	/*let query={
		crawler: "dummy-crawler",
		delivery: "none",
		pusherAppId: settings.pusher.appId,
		pusherKey: settings.pusher.key,
		pusherSecret: settings.pusher.secret,
		pusherCluster: settings.pusher.cluster,
		pusherChannel: "crawl-progress",
		crawlId: "123",
		"input-rows": 10,
		"input-cols": 3,
	};*/

	req={query: query};
	res=new DummyResponse();

	await runner.runCrawlRest(req,res);
}

main();
