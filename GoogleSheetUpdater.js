const {google}=require("googleapis");
const VerEx=require('verbal-expressions');
const EventEmitter=require("events");

class GoogleSheetUpdater extends EventEmitter {
	constructor(spreadsheetId) {
		super();
		this.spreadsheetId=spreadsheetId;
		this.appendRowIndex=0;
		this.rowsToAppend=[];
		this.appendRowsCall=null;
		this.objectKeys=null;
		this.interCallDelay=5000;
	}

	setInterCallDelay(delay) {
		this.interCallDelay=delay;
	}

	setClientIdAndSecret(clientId, secret) {
		this.clientId=clientId;
		this.secret=secret;
	}

	setRefreshToken(refreshToken) {
		this.refreshToken=refreshToken;
	}

	setTarget(target) {
		this.target=target;
	}

	getRowRange(row) {
		let exp=VerEx().removeModifier("g").digit().oneOrMore().endOfLine();

		if (!exp.test(this.target))
			throw new Error("Target is not on A1 notation");

		let firstRow=parseInt(exp.exec(this.target)[0]);
		return exp.replace(this.target,firstRow+row);
	}

	async init() {
		if (!this.clientId
				|| !this.secret
				|| !this.refreshToken
				|| !this.spreadsheetId
				|| !this.target)
			throw new Error("Need clientId, secret, refreshToken, spreadsheetId and target");

		let oauth2Client=new google.auth.OAuth2(this.clientId,this.secret);
		oauth2Client.setCredentials({
			refresh_token: this.refreshToken
		});

		let token=await oauth2Client.refreshAccessToken();
		if (!token.res.data)
			throw new Error("Got no access token from Google");

		oauth2Client.setCredentials(token.res.data);
		this.sheetsAPI=google.sheets({version: 'v4', auth: oauth2Client});
	}

	async updateRange(range, data, delay) {
		return new Promise((resolve,reject)=>{
			let params={
				spreadsheetId: this.spreadsheetId,
				range: range,
				resource: {
					range: range,
					majorDimension: "ROWS",
					values: data
				},
				valueInputOption: "RAW"
			};

			this.sheetsAPI.spreadsheets.values.update(params,(err,response)=>{
				if (err)
					reject(err);

				else {
					if (delay)
						setTimeout(resolve,delay);

					else
						resolve();
				}
			});
		});
	}

	appendRow(row) {
		if (this.flushPromise)
			throw new Error("appendRow can't be called after flush");

		this.rowsToAppend.push(row);
		this.sendAppendRows();
	}

	sendAppendRows() {
		if (this.appendRowsCall)
			return;

		if (!this.rowsToAppend.length)
			return;

		let range=this.getRowRange(this.appendRowIndex);
		let rows=this.rowsToAppend;
		this.rowsToAppend=[];
		this.appendRowIndex+=rows.length;

		//console.log("sending rows: "+rows.length);

		this.appendRowsCall=this.updateRange(range,rows,this.interCallDelay);
		this.appendRowsCall.then(()=>{
			this.appendRowsCall=null;
			this.sendAppendRows();
		});
		this.appendRowsCall.catch((e)=>{
			this.emit("error",e);
		});
	}

	async flush() {
		while (this.appendRowsCall)
			await Promise.resolve(this.appendRowsCall);
	}

	appendObject(o) {
		if (!this.objectKeys) {
			this.objectKeys=Object.keys(o);
			this.appendRow(this.objectKeys);
		}

		let a=[];
		for (let key of this.objectKeys)
			a.push(o[key]);

		this.appendRow(a);
	}
}

module.exports=GoogleSheetUpdater;