const Pusher=require("pusher");
const EventEmitter=require("events");

class PusherUpdater extends EventEmitter {
	constructor(pusherOptions, channel) {
		super();

		this.pusher=new Pusher(pusherOptions);
		this.promises=[];
		this.channel=channel;
	}

	setCrawlId(crawlId) {
		this.crawlId=crawlId;
	}

	send(message) {
		let promise=new Promise((resolve,reject)=>{
			this.pusher.trigger(this.channel,message._,message,
				(err, req, res)=>{
					if (err)
						reject(err);

					else
						resolve();
				}
			);
		});

		promise.catch((e)=>{
			console.log("pusher error!");
			this.emit("error",e);
		});

		this.promises.push(promise);
		return promise;
	}

	sendProgress(progress) {
		return this.send({
			_: "progress",
			progress: progress,
			crawlId: this.crawlId
		});
	}

	updateProgress(progress) {
		this.progress=progress;

		if (this.progressCall)
			return;

		this.progressCall=this.sendProgress(progress);
		this.progressCall.then(()=>{
			setTimeout(()=>{
				this.progressCall=null;
				if (this.progress!=progress)
					this.updateProgress(this.progress);
			},2500);
		});
	}

	async flush() {
		await Promise.resolve(Promise.all(this.promises));
	}
}

module.exports=PusherUpdater;
