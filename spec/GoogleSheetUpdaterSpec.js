const GoogleSheetUpdater=require("../GoogleSheetUpdater.js");

class MockSheetsAPI {
	constructor() {
		this.sent=[];
		this.spreadsheets={
			values: {
				update: (params,cb)=>{
					this.sent=[...this.sent,params.resource.values];
					console.log("sending rows: "+params.resource.values.length);
					setTimeout(()=>{
						console.log("  - done");
						cb();
					},1000); //();
				}
			}
		}
	}
}

async function sleep(delay) {
	await new Promise((resolve)=>{
		setTimeout(resolve,delay);
	});
}

describe("GoogleSheetUpdater",()=>{
	it("can update with objects",async ()=>{
		let updater=new GoogleSheetUpdater("123");

		updater.target="Test!A1";
		updater.sheetsAPI=new MockSheetsAPI();
		updater.setInterCallDelay(100);
		updater.appendRow({"a":1,"b":2});
		updater.appendRow({"a":2,"b":3});
		updater.appendRow({"a":2,"b":3});
		await sleep(100);
		updater.appendRow({"a":2,"b":3});
		updater.appendRow({"a":2,"b":3});
		await updater.flush();
		console.log("** exiting");
	});
});